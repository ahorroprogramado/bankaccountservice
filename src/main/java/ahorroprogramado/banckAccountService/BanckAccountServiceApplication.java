package ahorroprogramado.banckAccountService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanckAccountServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BanckAccountServiceApplication.class, args);
	}

}
